
package buzzworddata;

import apptemplate.AppTemplate;
import components.AppDataComponent;


public class BuzzwordData implements AppDataComponent{
    public AppTemplate appTemplate;
    public String username;
    public String password;
    public int DictionaryLevels;
    public int FamousLevels;
    public int placeLevels;



    public void newProfile(String username, String password){
        this.username=username;
        this.password=password;
        this.DictionaryLevels=0;
        this.FamousLevels=0;
        this.placeLevels=0;
    }


    @Override
    public void reset() {

    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password){
        this.password=password;
    }
    public void setDictionaryLevels(int level){
        this.DictionaryLevels = level;
    }
    public void setFamousLevels(int level){
        this.FamousLevels = level;
    }
    public void setPlaceLevels(int level){
        this.placeLevels = level;
    }


    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }
    public int getDictionaryLevels(){
        return  DictionaryLevels;
    }
    public int getFamousLevels(){
        return FamousLevels;
    }
    public int getPlaceLevels(){
        return placeLevels;
    }
    public BuzzwordData(AppTemplate appTemplate){this.appTemplate=appTemplate;}
}

