
package gui;

import static buzzword.BuzzwordProperties.*;
import apptemplate.AppTemplate;
import buzzword.Buzzword;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

public class BuzzwordWorkspace extends AppWorkspaceComponent {
    public AppTemplate appTemplate;
    public AppGUI gui;
    public Label guiHeadingLabel;
    public Stage current;
    public Button login;
    public Text title;
    public Button newProfile;
    public Scene scene;
    public Group root;
    public Button start;
    public Circle close;
    public Text x;
    public GridPane grid;
    public BuzzwordController controller;
    private static final int TOTAL_NUMBER_OF_STORED_WORDS = 330622;

    public BuzzwordWorkspace(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gui = appTemplate.getGUI();
        controller = (BuzzwordController) gui.getFileController();
        layoutGUI();
    }

    public void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));
        workspace = new VBox();
        Buzzword buzz = (Buzzword) appTemplate;
        makeHomeScreen(buzz.getStage());
    }

    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        //gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        // gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        /*ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));*/

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    @Override
    public void reloadWorkspace() {
    }

    ;

    public void setHandlers(int a) {
        if(a==1){
            close.setOnMouseClicked(event -> ensure());
            x.setOnMouseClicked(e -> ensure());
        }
        else{
            close.setOnMouseClicked(event -> current.close());
        }
        x.setOnMouseClicked(event -> current.close());
        login.setOnMouseClicked(event -> controller.loginAlert());
        start.setOnMouseClicked(event -> makeGameSelectScreen());
        newProfile.setOnMouseClicked(event -> {
            try {
                controller.newProfileAlert();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void makeHomeScreen(Stage current) {
        if (current != null) {
            current.close();
        }
        this.current = current;
        root = new Group();

        StackPane closing = new StackPane();
        x = new Text("X");
        close = new Circle();
        close.setRadius(10);
        close.setFill(Color.CADETBLUE);
        close.setStroke(Color.BLACK);
        closing.getChildren().addAll(close, x);
        closing.setTranslateX(650);
        closing.setTranslateY(10);

        HBox buttons = new HBox();
        login = new Button("Login");
        newProfile = new Button("Create new Profile");
        start = new Button("Start Playing");
        title = new Text("Buzzword");
        title.setFont(new Font("Copperplate Gothic Light", 70));
        title.setFill(Color.rgb(51, 107, 135));
        buttons.getChildren().addAll(login, newProfile, start);
        root.getChildren().addAll(title, buttons, closing);

        scene = new Scene(root, 700, 700, Color.rgb(144, 175, 197));

        current.setScene(scene);
        title.setTranslateX(160);
        title.setTranslateY(100);
        login.setFont(new Font("Copperplate Gothic Light", 14));
        login.setTranslateX(300);
        login.setTranslateY(400);
        newProfile.setTranslateX(180);
        newProfile.setTranslateY(450);
        newProfile.setFont(new Font("Copperplate Gothic Light", 14));
        start.setTranslateX(40);
        start.setTranslateY(500);
        start.setFont(new Font("Copperplate Gothic Light", 14));

        current.setTitle("Buzzword");
        setHandlers(2);
        current.show();
    }

    public void makeGameSelectScreen() {
        this.current.close();
        this.current = new Stage();
        root = new Group();

        StackPane closing = new StackPane();
        x = new Text("X");
        close = new Circle();
        close.setRadius(15);
        close.setFill(Color.CADETBLUE);
        close.setStroke(Color.BLACK);
        closing.getChildren().addAll(close, x);
        closing.setTranslateX(650);
        closing.setTranslateY(10);

        Button homeButton = new Button("Home");
        homeButton.setStyle("-fx-background-color: cadetblue");
        homeButton.setFont(Font.font("Copperplate Gothic Light", 20));
        homeButton.setOnMouseClicked(event -> makeHomeScreen(current));

        Text levelselect = new Text("Game Mode Select");
        levelselect.setFont(new Font("Copperplate Gothic Light", 50));
        levelselect.setFill(Color.rgb(51, 107, 135));
        StackPane levels = new StackPane();
        Circle famous = new Circle(80, Color.rgb(51, 107, 135));
        Circle dict = new Circle(80, Color.rgb(51, 107, 135));
        Circle places = new Circle(80, Color.rgb(51, 107, 135));

        Text fame = new Text("Famous People");
        Text dictionary = new Text("Dictionary Words");
        Text placenames = new Text("Place Names");

        fame.setFont(Font.font("Copperplate Gothic Light"));
        dictionary.setFont(Font.font("Copperplate Gothic Light"));
        placenames.setFont(Font.font("Copperplate Gothic Light"));

        famous.setTranslateY(400);
        famous.setTranslateX(80);
        fame.setTranslateY(400);
        fame.setTranslateX(80);
        dict.setTranslateX(280);
        dict.setTranslateY(400);
        dictionary.setTranslateX(280);
        dictionary.setTranslateY(400);
        places.setTranslateX(480);
        places.setTranslateY(400);
        placenames.setTranslateX(480);
        placenames.setTranslateY(400);

        levelselect.setTranslateX(100);
        levelselect.setTranslateY(100);
        famous.setOnMousePressed(event -> famous.setFill(Color.AQUA));
        dict.setOnMousePressed(event -> dict.setFill(Color.AQUA));
        places.setOnMousePressed(event -> places.setFill(Color.AQUA));
        famous.setOnMouseClicked(event -> makeLevelSelectScreen("Famous People"));
        fame.setOnMouseClicked(event -> makeLevelSelectScreen("Famous People"));
        dict.setOnMouseClicked(event -> makeLevelSelectScreen("Dictionary Words"));
        dictionary.setOnMouseClicked(event -> makeLevelSelectScreen("Dictionary Words"));
        places.setOnMouseClicked(event -> makeLevelSelectScreen("Place Names"));
        placenames.setOnMouseClicked(event -> makeLevelSelectScreen("Place Names"));
        levels.getChildren().addAll(famous, fame, dict, dictionary, places, placenames);
        root.getChildren().addAll(levels, levelselect, closing, homeButton);
        scene = new Scene(root, 700, 700, Color.rgb(144, 175, 197));
        current.setScene(scene);
        current.setTitle("Buzzword");
        setHandlers(2);
        current.show();

    }

    public void makeLevelSelectScreen(String category) {
        this.current.close();
        current = new Stage();
        root = new Group();
        Button homeButton = new Button("Home");
        homeButton.setStyle("-fx-background-color: cadetblue");
        homeButton.setFont(Font.font("Copperplate Gothic Light", 20));
        homeButton.setOnMouseClicked(event -> makeHomeScreen(current));
        StackPane closing = new StackPane();
        x = new Text("X");
        close = new Circle();
        close.setRadius(15);
        close.setFill(Color.CADETBLUE);
        close.setStroke(Color.BLACK);
        closing.getChildren().addAll(close, x);
        closing.setTranslateX(650);
        closing.setTranslateY(10);
        Button Profile = new Button("View Profile");

        StackPane levels = new StackPane();
        Text levelselect = new Text(category + " Level Select");
        levelselect.setFont(new Font("Copperplate Gothic Light", 30));
        levelselect.setFill(Color.rgb(51, 107, 135));
        int levelcounter=0;
        for (int i = 1; i <= 8; i++) {
            Circle temp = new Circle(20, Color.rgb(51, 107, 135));
            Text text = new Text("" + i);
            temp.setTranslateX(i * 70);
            temp.setTranslateY(500);
            text.setFont(Font.font("Copperplate Gothic Light"));
            text.setTranslateX((i * 70));
            text.setTranslateY(500);
            temp.setOnMousePressed(event -> temp.setFill(Color.AQUA));
            temp.setOnMouseReleased(event -> temp.setFill(Color.rgb(51,107,135)));
            int finalI = i;
            String categ=category;
            if(levelcounter==0) {
                temp.setOnMouseClicked(event -> makeGameplayScreen(finalI, categ));
                text.setOnMouseClicked(event -> makeGameplayScreen(finalI, categ));
            }
            levelcounter++;
            levels.getChildren().addAll(temp, text);
        }
        levelselect.setTranslateX(100);
        levelselect.setTranslateY(100);
        homeButton.setAlignment(Pos.TOP_LEFT);
        root.getChildren().addAll(levelselect, levels, closing, homeButton);
        scene = new Scene(root, 700, 700, Color.rgb(144, 175, 197));
        current.setScene(scene);
        current.setTitle("Buzzword");
        setHandlers(2);
        current.show();
    }

    public int RandomPlacement() {
        return new Random().nextInt(4)+1;
    }

    public String placement() {
        int x = RandomPlacement();
        if (x == 1)
            return "left";
        else if (x == 2)
            return "right";
        else if (x == 3)
            return "top";
        else
            return "bottom";

    }

    public void makeGameplayScreen(int level,String category) {
        this.current.close();
        current = new Stage();
        root = new Group();
        HBox rooted = new HBox();
        TextField input = new TextField();
        input.setPromptText("Current Guess");
        Text gameplayscreen = new Text("Buzzword " + category+ ": " +level);
        gameplayscreen.setFont(new Font("Copperplate Gothic Light", 60));
        gameplayscreen.setFill(Color.rgb(51, 107, 135));

        grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(100, 150, 500, 10));
        String word ="test";
        int row = 0;
        int column = 0;


        Text text = new Text(word.substring(0, 1));
        text.setFont(Font.font("Copperplate Gothic Light", 30));
        LetterNode first = new LetterNode(null, null, null, null, text);
        grid.add(first.getCircle(), column, row);
        int onevalue =0;
        int twovalue =0;
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
       /* for(int i =0;i<4;i++){
            for(int a=0;a<4;a++) {
                if(onevalue==3){
                    twovalue++;
                    onevalue=0;
                }
                int position = new Random().nextInt(24)+1;
                String letter= alphabet.substring(position,position +1);
                Text b = new Text(letter);
                Circle shape = new Circle(20,Color.rgb(51,107,135));
                shape.setRadius(50);
                StackPane temporary= new StackPane();
                Circle finalA = shape;
                shape.setOnMousePressed(event -> finalA.setFill(Color.AQUA));
                shape.setOnMouseReleased(event -> finalA.setFill(Color.rgb(51,107,135)));
                b.setOnMousePressed(event -> finalA.setFill(Color.AQUA));
                b.setOnMouseReleased(event -> finalA.setFill(Color.rgb(51,107,135)));
                temporary.getChildren().addAll(finalA,b);

                b.setFont(Font.font("Copperplate Gothic Light", 30));
                grid.add(temporary,onevalue,twovalue);
                onevalue++;
            }
        }*/
        boolean taken[][]= new boolean[4][4];
        for(int i =0;i<4;i++){
            for(int a=0;a<4;a++) {
                taken[i][a]=false;
            }
        }


        LetterNode tempx = new LetterNode();
        boolean added=false;
        int counter=1;


       int rowss=0;
        int columnss =0;


        for(int i=0;i<16;i++){
            if(columnss==4){
                columnss=0;
                rowss++;
            }
            Circle a = new Circle(20,Color.rgb(51,107,135));
            a.setRadius(50);
            StackPane temp = new StackPane();
            temp.getChildren().add(a);
            grid.add(temp,columnss,rowss);
            columnss++;
        }

        for (int i = 1; i < 16; i++) {
            if(i+1==word.length()){
                word=setTargetWord();
                counter=0;
            }
            added = false;
            text = new Text(word.substring(counter,counter+1));
           String placement = placement();

            if (placement.equals("left")) {
                if (column - 1 > 0) {
                    column--;
                    text.setFont(Font.font("Copperplate Gothic Light", 30));
                    tempx = new LetterNode(null, first, null, null, text);
                    first.setLeft(tempx);
                    taken[column][row]=true;
                    added=true;
                } else {
                    column++;
                    text.setFont(Font.font("Copperplate Gothic Light", 30));
                    tempx = new LetterNode(first, null, null, null, text);
                    first.setRight(tempx);
                    taken[column][row]=true;
                    added=true;
                }
            }


            else if (placement.equals("right")) {
                if (column + 1 < 3) {

                    text.setFont(Font.font("Copperplate Gothic Light", 30));
                    column++;
                    tempx = new LetterNode(first, null, null, null, text);
                    first.setRight(tempx);
                    taken[column][row]=true;
                    added=true;
                } else {
                    column--;
                    tempx = new LetterNode(null, first, null, null, text);
                    text.setFont(Font.font("Copperplate Gothic Light", 30));
                    first.setLeft(tempx);
                    added=true;
                    taken[column][row]=true;
                }
            }

            else if (placement.equals("top")) {
                if (row - 1 > 0) {

                    row--;
                    text.setFont(Font.font("Copperplate Gothic Light", 30));
                    tempx = new LetterNode(null, null, null, first, text);
                    first.setTop(tempx);
                    taken[column][row]=true;
                    added=true;
                } else {
                    row++;
                    text.setFont(Font.font("Copperplate Gothic Light", 30));
                    tempx = new LetterNode(null, null, first, null, text);
                    first.setBottom(tempx);
                    taken[column][row]=true;
                    added=true;
                }
            }
            else if (placement.equals("bottom")) {
                if (row + 1 < 3) {

                    text.setFont(Font.font("Copperplate Gothic Light", 30));
                    row++;
                    tempx = new LetterNode(null, null, first, null, text);
                    first.setBottom(tempx);
                    taken[column][row]=true;
                    added=true;

                } else {
                    row--;
                    text.setFont(Font.font("Copperplate Gothic Light", 30));
                    tempx = new LetterNode(null, null, null, first, text);
                    first.setBottom(tempx);
                    taken[column][row]=true;
                    added=true;
                }
            }
            if(tempx==null || row<0 || row>3 || column<0 || column>3||!added){
                i--;
            }
            else {
                first = tempx;
                grid.add(tempx.getCircle(), column, row);
            }
        }
        String oneword = setTargetWord();
        columnss= new Random().nextInt(2)+1;
        rowss=0;
        for(int i=0;i<4;i++){
            if(columnss==4){
                columnss=0;
                rowss++;
            }
            Circle a = new Circle(20,Color.rgb(51,107,135));
            a.setRadius(50);
            Text txt = new Text(oneword.substring(i,i+1));
            StackPane temp = new StackPane();
            txt.setFont(Font.font("Copperplate Gothic Light", 30));
            temp.getChildren().addAll(a,txt);
            grid.add(temp,columnss,rowss);
            if(i%2==0 &&columnss+1<4)
            columnss++;
            else
                rowss++;
        }

        VBox screen = new VBox();


        VBox information = new VBox();
        Label timer = new Label("Time Remaining: 60 seconds");
        Label TargetScore = new Label("Target Score: 400");
        Label info = new Label("Each letter for a whole word found will give you 100 points");
        info.setFont(Font.font("Copperplate Gothic Light"));
        info.setTextFill(Color.rgb(51, 107, 135));
        Label currentScore = new Label("Current Score: 0");
        timer.setFont(Font.font("Copperplate Gothic Light", 20));
        timer.setTextFill(Color.rgb(51, 107, 135));
        TargetScore.setFont(Font.font("Copperplate Gothic Light"));
        TargetScore.setTextFill(Color.rgb(51, 107, 135));
        currentScore.setFont(Font.font("Copperplate Gothic Light"));
        currentScore.setTextFill(Color.rgb(51, 107, 135));
        TableView table = new TableView();

        TableColumn wordcol = new TableColumn("Words");
        wordcol.setResizable(false);
        wordcol.setStyle("-fx-background-color: #90AFC5");
        TableColumn pointcol = new TableColumn("Points Earned");
        pointcol.setResizable(false);
        pointcol.setStyle("fx-background-color: #90AFC5");

        wordcol.prefWidthProperty().bind(table.widthProperty().multiply(.5));
        pointcol.prefWidthProperty().bind(table.widthProperty().multiply(.5));
        table.getColumns().addAll(wordcol, pointcol);
        table.setMaxWidth(500);

        table.setStyle("-fx-background-color: #336b87");


        StackPane closing = new StackPane();

        close = new Circle();
        close.setRadius(15);
        close.setFill(Color.CADETBLUE);
        close.setStroke(Color.BLACK);
        closing.getChildren().addAll(close, x);
        closing.setTranslateX(1050);
        closing.setTranslateY(10);

        Button pause = new Button("Pause");
        pause.setTranslateY(10);
        pause.setTranslateX(950);
        pause.setOnMouseClicked(event -> Pause());
        information.getChildren().addAll(timer, TargetScore,info, currentScore, table);

        HBox temp = new HBox();
        temp.getChildren().addAll(grid, information);
        screen.getChildren().addAll(gameplayscreen, temp);
        screen.setAlignment(Pos.CENTER);
        input.setTranslateY(540);
        input.setTranslateX(600);
        rooted.getChildren().addAll(screen);
        root.getChildren().addAll(rooted, closing, input, pause);
        scene = new Scene(root, 1100, 700, Color.rgb(144, 175, 197));
        current.setScene(scene);
        current.setTitle("Buzzword");
        setHandlers(1);
        close.setOnMouseClicked(e -> ensure());
        x.setOnMouseClicked(e -> ensure());
        current.show();
    }


    private String setTargetWord() {
        URL wordsResource = getClass().getClassLoader().getResource("words/simple.txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(20);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            String word = lines.skip(toSkip).findFirst().get();
            if (word.length() < 3) {
                return setTargetWord();
            }
            for (int i = 0; i < word.length(); i++) {
                if (Character.isLetter(word.charAt(i)) != true)
                    return this.setTargetWord();
            }
            return word;
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return null;
    }
    public void ensure() {
        ButtonType returned = new ButtonType("return");
        ButtonType closee = new ButtonType("close");
        grid.setVisible(false);
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setResizable(false);
        dialog.setHeight(700);
        dialog.getButtonTypes().setAll(returned,closee);
        dialog.setWidth(700);
        dialog.setTitle("Are you sure?");
        dialog.setHeaderText("You have pressed the X button.");
        dialog.setContentText("If you wish to exit, press close. If you wish to return, press return.");
        Optional<ButtonType> result = dialog.showAndWait();
        if(result.get()==closee){
        current.close();}
        else if(result.get()==returned){
            dialog.close();
        }
        grid.setVisible(true);
    }

    public void Pause() {
        grid.setVisible(false);
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setResizable(false);
        dialog.setHeight(700);
        dialog.setWidth(700);
        dialog.setTitle("Game Paused");
        dialog.setHeaderText("Game Paused");
        dialog.setContentText("You have paused the game. Press OK to resume.");
        dialog.showAndWait();
        grid.setVisible(true);
    }
}



